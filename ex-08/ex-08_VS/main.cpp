//Nome: Thiago Henrique Frois Menon Cunha
//Data: 21/03/2022

#include "Principal.hpp"

int main()
{
    Principal principal;

    principal.executar();

    return 0;
}
